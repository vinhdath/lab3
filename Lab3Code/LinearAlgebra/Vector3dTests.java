// Vinh Dat Hoang
// 0720203
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
    
   public class Vector3dTests {
        
        // Testing each method
        @Test
        public void testGetX() {
            Vector3d vector = new Vector3d(1,2,3);
            assertEquals(vector.getX(),1);   
        }
        @Test
        public void testGetY() {
            Vector3d vector = new Vector3d(1,2,3);
            assertEquals(vector.getY(),2);
        }
        @Test
        public void testGetZ() {
            Vector3d vector = new Vector3d(1,2,3);
            assertEquals(vector.getZ(),3);
        }
        @Test
        public void testMagnitude() {
            Vector3d vector = new Vector3d(1,2,3);
            assertTrue((vector.magnitude() - 3.7416) < 1) ;
        }
        @Test
        public void testDotProduct() {
            Vector3d vector = new Vector3d(1,2,3);
            Vector3d vector2 = new Vector3d(4,5,6);
            assertEquals(vector.dotProduct(vector2.getX(),vector2.getY(),vector2.getZ()),32) ;
        }
        @Test
        public void testAdd() {
            Vector3d vector = new Vector3d(1,2,3);
            Vector3d vector2 = new Vector3d(4,5,6);
            Vector3d vector3 = vector.add(vector2.getX(),vector2.getY(),vector2.getZ());
            assertEquals(vector3.getX(),5);
            assertEquals(vector3.getY(),7);
            assertEquals(vector3.getZ(),9);
        }
    } 

