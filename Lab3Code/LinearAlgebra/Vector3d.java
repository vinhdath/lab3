// Vinh Dat Hoang
// ID: 0720203
package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y,2) + Math.pow(this.z,2));
    }
    public double dotProduct(double r, double t, double s){
        return (this.x * r) + (this.y * t) + (this.z * s);
    }
    public Vector3d add(double r, double t, double s){
        return new Vector3d ((this.x + r), (this.y + t), (this.z + s));
    }
    public String toString(){
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }
}
